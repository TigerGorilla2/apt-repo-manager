# This is a comment
FROM debian:bookworm

ENV root /src
ENV incoming /incoming
ENV apt_repo /apt-repo

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR $root
COPY . $root

RUN apt-get update \
    && apt-get install -y apt-utils gpg

RUN gpg --batch --gen-key $root/gpg-key-info

# Default to doing nothing and just staying alive
CMD ./update-repo.sh
