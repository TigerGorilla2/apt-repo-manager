#!/bin/bash -ex
# Update the local apt repository

# Prevents external parties to remove packages
cp --recursive --update $incoming/* $apt_repo/pool/

gpg --armor --export > $apt_repo/key.asc
cd $apt_repo
while read -r dist; do
  # every distribution e.g. bookworm, bullseye
  while read -r component; do
    # every component e.g. main, contrib, or in my custom case local
    while read -r arch; do
      # every architecture e.g. binary-amd64, binary-i386, binary-arm64
      apt-ftparchive packages "pool/$arch" > "dists/$dist/$component/$arch/Packages"
      gzip -kf "dists/$dist/$component/$arch/Packages"
    done < <(find dists/$dist/$component -mindepth 1 -maxdepth 1 -type d -exec basename -a '{}' +)
  done < <(find dists/$dist -mindepth 1 -maxdepth 1 -type d -exec basename -a '{}' +)
  # again at every distribution e.g. bookworm, bullseye
  apt-ftparchive release -o APT::FTPArchive::Release::Codename=$dist "dists/$dist" > "dists/$dist/Release"
  gpg --yes -abo "dists/$dist/Release.gpg" "dists/$dist/Release"
  gpg --yes --clearsign -o "dists/$dist/InRelease" "dists/$dist/Release"
done < <(find dists -mindepth 1 -maxdepth 1 -type d -exec basename -a '{}' +)
