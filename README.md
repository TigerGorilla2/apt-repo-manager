# Maintain an APT-Repository in a docker container

Ofcourse, instead of `docker` you can use `podman` like
> `alias docker='podman'`

## Build the image
`docker build -t apt-repo-manager .`

## Building a(pt-)r(epo-)m(anag)er
Replace `src=incoming` with the folder where you will put new deb-packges and
`src=apt-repo` with the folder that is accessible via e.g. http (this is what
you use in apts `.sources` files)
```sh
docker run \
    --mount type=bind,src=incoming,target=/incoming \
    --mount type=bind,src=apt-repo,target=/apt-repo \
    --name armer \
    localhost/apt-repo-manager
```

To update the repo just start the container again
```sh
docker start --latest \
    --filter name=armer
```
